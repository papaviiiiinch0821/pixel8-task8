<?php
include '../includes/connection.php';

// Check if the updateEmployeeButton is set
if(isset($_POST['updateEmployeeButton'])){
    $id = $_POST['editEmployeeID']; // No need to escape, MysqliDb handles SQL injection
    $first_name = $_POST['editEmployeeFirstName'];
    $last_name = $_POST['editEmployeeLastName'];
    $middle_name = $_POST['editEmployeeMiddleName'];
    $birthday = $_POST['editEmployeeBirthday'];
    $address = $_POST['editEmployeeAddress'];

    // Check if any required field is empty
    if (empty($first_name) || empty($last_name) || empty($middle_name) || empty($birthday) || empty($address)) {
        // Response Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Please complete the employee details to be updated.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }

    // Update the employee details using MysqliDb's update method
    $data = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'birthday' => $birthday,
        'address' => $address
    ];
    $db->where('id', $id);
    $result = $db->update('employee', $data);

    if($result) {
        // Response Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'Employee details updated successfully.'
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Response Status and Message Response
        $res = [
            'status' => 401, // Error Number
            'message' => 'Employee details could not be updated at the moment.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}
?>
