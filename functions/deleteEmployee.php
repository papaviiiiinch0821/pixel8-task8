<?php
include '../includes/connection.php';

// Check if the deleteEmployeeButton is set
if (isset($_POST['deleteEmployeeButton'])){
    $id = $_POST['deleteEmployeeID'];

    // Delete the employee using MysqliDb's delete method
    $db->where('id', $id);
    $result = $db->delete('employee');

    if ($result) {
        // Response Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'Employee deleted successfully.'
        ];
        // Display message
        echo json_encode($res);
        return false;
    }
    else {
        // Response Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Employee could not be deleted at this moment. Please try again later.'
        ];
        // Display message
        echo json_encode($res);
        return false;
    }
}
?>
