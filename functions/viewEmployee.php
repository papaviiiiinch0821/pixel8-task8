<?php
include '../includes/connection.php';

if(isset($_GET['employeeID'])){
    $id = $_GET['employeeID']; 

    // Fetch data using MysqliDb
    $db->where('id', $id);
    $employee = $db->getOne('employee');

    if ($db->count == 1) {
        // Respone Status and Message Response
        $res = [
            'status' => 200, 
            'message' => 'Employee fetched successfully by id.',
            'data' => $employee 
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Respone Status and Message Response
        $res = [
            'status' => 400, 
            'message' => 'Employee details not found.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}
?>
