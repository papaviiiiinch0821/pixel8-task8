<?php
include '../includes/connection.php';

// Check if the createEmployeeButton is set
if(isset($_POST['createEmployeeButton'])){
    $first_name = $_POST['createEmployeeFirstName']; // No need to escape, MysqliDb handles SQL injection
    $last_name = $_POST['createEmployeeLastName'];
    $middle_name = $_POST['createEmployeeMiddleName'];
    $birthday = $_POST['createEmployeeBirthday'];
    $address = $_POST['createEmployeeAddress'];

    if ($first_name == NULL || $last_name == NULL || $middle_name == NULL || $birthday == NULL || $address == NULL){
        // Response Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Please complete the employee details to be created.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }

    // Build data array for insertion
    $data = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'birthday' => $birthday,
        'address' => $address
    ];

    // Insert data using MysqliDb
    $id = $db->insert('employee', $data);

    if($id) {
        // Response Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'New employee created.'
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Response Status and Message Response
        $res = [
            'status' => 401, // Error Number
            'message' => 'Employee could not be created at the moment.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}
?>
