// Create Employee
$("#createEmployeeForm").on("submit", function (e) {
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("createEmployeeButton", true);

  $.ajax({
    type: "POST",
    url: "./functions/createEmployee.php",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      // console.log(response);
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);

        setTimeout(() => {
          // Hide Modal
          $("#createEmployeesModal").modal("hide");
          // Reload Table
          $("#employeeTable").load(" #employeeTable > *");
          // Reset Form
          $("#createEmployeeForm")[0].reset();
        }, 2000);
      } else if (res.status == 400 || res.status == 401) {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});

// View Employee
$(document).on("click", ".viewEmployeeButton", function () {
  var employeeID = $(this).val();
  $.ajax({
    type: "GET",
    url: "./functions/viewEmployee.php?employeeID=" + employeeID,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        $("#viewEmployeeID").val(res.data.id);
        $("#viewEmployeeFirstName").val(res.data.first_name);
        $("#viewEmployeeLastName").val(res.data.last_name);
        $("#viewEmployeeMiddleName").val(res.data.middle_name);
        $("#viewEmployeeBirthday").val(res.data.birthday);
        $("#viewEmployeeAddress").val(res.data.address);
        $("#viewEmployeesModal").modal("show");
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});

// Edit Employee
$(document).on("click", ".editEmployeeButton", function () {
  var employeeID = $(this).val();
  $.ajax({
    type: "GET",
    url: "./functions/viewEmployee.php?employeeID=" + employeeID,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        $("#editEmployeeID").val(res.data.id);
        $("#editEmployeeFirstName").val(res.data.first_name);
        $("#editEmployeeLastName").val(res.data.last_name);
        $("#editEmployeeMiddleName").val(res.data.middle_name);
        $("#editEmployeeBirthday").val(res.data.birthday);
        $("#editEmployeeAddress").val(res.data.address);
        $("#editEmployeesModal").modal("show");
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});

// Update Employee
$("#editEmployeeForm").on("submit", function (e) {
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("updateEmployeeButton", true);

  $.ajax({
    type: "POST",
    url: "./functions/updateEmployee.php",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      // console.log(response);
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);

        setTimeout(() => {
          // Hide Modal
          $("#editEmployeesModal").modal("hide");
          // Reload Table
          $("#employeeTable").load(" #employeeTable > *");
          // Reset Form
          $("#editEmployeeForm")[0].reset();
        }, 2000);
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});

//  Confirm Delete Employee
$(document).on("click", ".deleteEmployeeButton", function () {
  var employeeID = $(this).val();
  $.ajax({
    type: "GET",
    url: "./functions/viewEmployee.php?employeeID=" + employeeID,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        $("#deleteEmployeeID").val(res.data.id);
        $("#deleteEmployeeFirstName").text(res.data.first_name);
        $("#deleteEmployeeLastName").text(res.data.last_name);
        $("#deleteEmployeesModal").modal("show");
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});

// Delete Employee
$("#deleteEmployeeForm").on("submit", function (e) {
  e.preventDefault();
  formData = new FormData(this);
  formData.append("deleteEmployeeButton", true);

  $.ajax({
    type: "POST",
    url: "./functions/deleteEmployee.php",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      var res = jQuery.parseJSON(response);

      if (res.status == 200) {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.success(res.message);
        setTimeout(() => {
          $("#deleteEmployeesModal").modal("hide");
          $("#employeeTable").load(" #employeeTable > *");
        }, 2000);
      } else {
        // Notify User
        alertify.set("notifier", "position", "top-right");
        alertify.error(res.message);
      }
    },
  });
});
