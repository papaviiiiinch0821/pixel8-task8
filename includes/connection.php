<?php
$servername = 'localhost';
$username = 'root';
$password = '';
$dbname = "p8_exercise_backend";

require_once 'MysqliDb.php';

// Initialize MysqliDb object with your database connection details
$db = new MysqliDb($servername, $username, $password, $dbname);


// MysliDb already checks database connection so there's no need to set up another validation here


// // Check if the connection was successful
// if ($db->connect()) {
//     die("Connection failed: " . $db->getLastError());
// }

// // Connection successful
// echo "Connected successfully";
?>
